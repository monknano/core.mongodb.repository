﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core.MongoDB.Repository.Interfaces
{
	public interface IMongoContext
	{
		IMongoClient Client { get; }
		IMongoDatabase Database { get; }
	}
}
