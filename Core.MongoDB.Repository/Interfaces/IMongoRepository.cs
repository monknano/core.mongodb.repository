﻿using Core.MongoDB.Repository.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core.MongoDB.Repository.Interfaces
{
	public interface IMongoRepository<T, in TKey> : IQueryable<T>
		where T : IMongoEntity<TKey>
	{
		IMongoCollection<T> MongoCollection { get; }

		void AscendingIndex(Expression<Func<T, object>> predicate);

		void FullTextindex(Expression<Func<T, object>> predicate);

		void DescendingIndex(Expression<Func<T, object>> predicate);

		IQueryable<T> AsQueryable();

		T GetById(TKey id);

		T GetByGuid(string guid);

		T Add(T entity);

		void AddMany(IEnumerable<T> entities);

		Task AddAsync(T entity);

		Task AddManyAsync(IEnumerable<T> entities);

		T AddTo<Key>(T entity, Expression<Func<T, IEnumerable<Key>>> sel, Key value);

		T Update(T entity);

		Task<T> UpdateAsync(T entity);

		void UpdateAsync(IEnumerable<T> entities);

		T UpdateByGuid(T entity);

		Task<T> UpdateByGuidAsync(T entity);

		void UpdateByGuid(IEnumerable<T> entities);

		void Update(IEnumerable<T> entities);

		Task DeleteAsync(TKey id);

		Task DeleteAsync(T entity);

		Task DeleteAsync(Expression<Func<T, bool>> predicate);

		void Delete(TKey id);

		void Delete(T entity);

		void Delete(Expression<Func<T, bool>> predicate);

		void DeleteAll();

		Task DeleteAsync();

		long Count(Expression<Func<T, bool>> predicate);

		IEnumerable<T> SortBy(Expression<Func<T, object>> predicate);

		IEnumerable<T> SortByDescending(Expression<Func<T, object>> predicate);

		void Drop();

		long Count();

		bool Exists();

		bool Exists(Expression<Func<T, bool>> predicate);

		IEnumerable<T> Search(string search);

		IEnumerable<T> Where(Expression<Func<T, bool>> predicate);

		int Sum(Func<T, int> sum);
	}

	public interface IMongoRepository<T> : IMongoRepository<T,string> where T : IMongoEntity<string>
	{
	}

}
