﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.MongoDB.Repository.Interfaces
{
	public interface IMongoEntity<Tkey>
	{
		[BsonId]
		Tkey Id { get; set; }

		string GuId { get; set; }
	}

	public interface IMongoEntity:IMongoEntity<string>
	{

	}
}
