﻿using Core.MongoDB.Repository.Attributes;
using Core.MongoDB.Repository.Interfaces;
using Core.MongoDB.Repository.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Core.MongoDB.Repository
{
	public class MongoRepository<TEntity, TKey> : IMongoRepository<TEntity, TKey> where TEntity : IMongoEntity<TKey>
	{
		public MongoRepository(IMongoContext context)
		{
			var type = typeof(TEntity);
			MongoCollection = context.Database.GetCollection<TEntity>(GetCollectionName());

			foreach (var prop in type.GetProperties())
			{
				if (prop.GetCustomAttribute(typeof(AscendingIndexAttribute)) != null)
				{
					var option = new CreateIndexOptions() { Unique = true };
					MongoCollection.Indexes.CreateOne(
						Builders<TEntity>.IndexKeys.Ascending(new StringFieldDefinition<TEntity>(prop.Name)),option);
				}

				if (prop.GetCustomAttribute(typeof(DescendingIndexAttribute)) != null)
				{
					MongoCollection.Indexes.CreateOne(
						Builders<TEntity>.IndexKeys.Descending(new StringFieldDefinition<TEntity>(prop.Name)));
				}

				if (prop.GetCustomAttribute(typeof(FullTextIndexAttribute)) != null)
				{
					MongoCollection.Indexes.CreateOne(Builders<TEntity>.IndexKeys.Text(new StringFieldDefinition<TEntity>(prop.Name)));
				}
			}
		}

		public IMongoCollection<TEntity> MongoCollection { get; }

		#region GetCollectionName
		private static string GetCollectionName()
		{
			var collectionName = typeof(TEntity).GetTypeInfo().BaseType == typeof(object)
				? GetCollectioNameFromInterface()
				: GetCollectionNameFromType(typeof(TEntity));

			if (string.IsNullOrEmpty(collectionName))
				throw new ArgumentException("Collection name cannot be empty for this entity");
			return collectionName;
		}

		private static string GetCollectioNameFromInterface()
		{
			// Check to see if the object (inherited from Entity) has a CollectionName attribute
			var att = typeof(TEntity).GetTypeInfo().GetCustomAttribute<CollectionNameAttribute>();
			var collectionname = att != null ? att.Name : typeof(TEntity).Name;

			return collectionname;
		}

		private static string GetCollectionNameFromType(Type entitytype)
		{
			string collectionname;

			// Check to see if the object (inherited from Entity) has a CollectionName attribute
			var att = entitytype.GetTypeInfo().GetCustomAttribute<CollectionNameAttribute>();
			if (att != null)
			{
				// It does! Return the value specified by the CollectionName attribute
				collectionname = att.Name;
			}
			else
			{
				if (typeof(MongoEntity).GetTypeInfo().IsAssignableFrom(entitytype))
					while (entitytype.GetTypeInfo().BaseType != typeof(MongoEntity))
						entitytype = entitytype.GetTypeInfo().BaseType;
				collectionname = entitytype.Name;
			}

			return collectionname;
		}
		#endregion

		public void AscendingIndex(Expression<Func<TEntity, object>> func)
		{
			MongoCollection.Indexes.CreateOne(Builders<TEntity>.IndexKeys.Ascending(func));
		}

		public void FullTextindex(Expression<Func<TEntity, object>> func)
		{
			MongoCollection.Indexes.CreateOne(Builders<TEntity>.IndexKeys.Text(func));
		}

		public void DescendingIndex(Expression<Func<TEntity, object>> func)
		{
			MongoCollection.Indexes.CreateOne(Builders<TEntity>.IndexKeys.Descending(func));
		}

		public virtual IQueryable<TEntity> AsQueryable()
		{
			return MongoCollection.AsQueryable();
		}

		public virtual TEntity GetById(TKey id)
		{
			var filter = Builders<TEntity>.Filter.Eq(s => s.Id, id);
			return MongoCollection.Find(filter).SingleOrDefault();
		}

		public virtual TEntity GetByGuid(string guid)
		{
			var filter = Builders<TEntity>.Filter.Eq(s => s.GuId, guid);
			return MongoCollection.Find(filter).SingleOrDefault();
		}

		public virtual TEntity Add(TEntity entity)
		{
			MongoCollection.InsertOne(entity);
			return entity;
		}

		public void AddMany(IEnumerable<TEntity> entities)
		{
			MongoCollection.InsertMany(entities);
		}

		public virtual async Task AddAsync(TEntity entity)
		{
			await MongoCollection.InsertOneAsync(entity);
		}

		public virtual async Task AddManyAsync(IEnumerable<TEntity> entities)
		{
			await MongoCollection.InsertManyAsync(entities);
		}

		public virtual TEntity AddTo<Key>(TEntity entity, Expression<Func<TEntity,IEnumerable<Key>>> sel, Key value)
		{
			var filter = Builders<TEntity>.Filter.Eq(x => x.Id, entity.Id);
			var update = Builders<TEntity>.Update.AddToSet(sel, value);
			MongoCollection.UpdateOne(filter, update);
			return entity;
		}

		public virtual TEntity Update(TEntity entity)
		{
			var filer = Builders<TEntity>.Filter.Eq(s => s.Id, entity.Id);
			MongoCollection.ReplaceOne(filer, entity);
			return entity;
		}

		public virtual async Task<TEntity> UpdateAsync(TEntity entity)
		{
			var filter = Builders<TEntity>.Filter.Eq(s => s.Id, entity.Id);
			await MongoCollection.ReplaceOneAsync(filter, entity);
			return entity;
		}

		public virtual TEntity UpdateByGuid(TEntity entity)
		{
			var filter = Builders<TEntity>.Filter.Eq(s => s.GuId, entity.GuId);
			MongoCollection.ReplaceOne(filter, entity);
			return entity;
		}

		public virtual async Task<TEntity> UpdateByGuidAsync(TEntity entity)
		{
			var filter = Builders<TEntity>.Filter.Eq(s => s.GuId, entity.GuId);
			await MongoCollection.ReplaceOneAsync(filter, entity);
			return entity;
		}

		public virtual void Update(IEnumerable<TEntity> entities)
		{
			Parallel.ForEach(entities, entity =>
			 {
				 Update(entity);
			 });
		}

		public virtual void UpdateAsync(IEnumerable<TEntity> entities)
		{
		  	Parallel.ForEach(entities, async entity =>
			{
				await UpdateAsync(entity);
			});
		}

		public virtual void UpdateByGuid(IEnumerable<TEntity> entities)
		{
			Parallel.ForEach(entities, entity =>
			{
				UpdateByGuid(entity);
			});
		}

		public virtual void Delete(TKey id)
		{
			var filter = Builders<TEntity>.Filter.Eq(s => s.Id, id);
			MongoCollection.DeleteOne(filter);
		}

		public virtual async Task DeleteAsync(TKey id)
		{
			var filter = Builders<TEntity>.Filter.Eq(s => s.Id, id);
			await MongoCollection.DeleteOneAsync(filter);
		}

		public virtual void Delete(TEntity entity)
		{
			var filter = Builders<TEntity>.Filter.Eq(s => s.Id, entity.Id);
			MongoCollection.DeleteOne(filter);
		}

		public virtual async Task DeleteAsync(TEntity entity)
		{
			var filter = Builders<TEntity>.Filter.Eq(s => s.Id, entity.Id);
			await MongoCollection.DeleteOneAsync(filter);
		}

		public virtual void Delete(Expression<Func<TEntity, bool>> predicate)
		{
			MongoCollection.DeleteMany<TEntity>(predicate);
		}

		public virtual async Task DeleteAsync(Expression<Func<TEntity,bool>> predicate)
		{
			await MongoCollection.DeleteManyAsync<TEntity>(predicate);
		}

		public virtual void DeleteAll()
		{
			MongoCollection.DeleteMany(x => true);
		}

		public virtual async Task DeleteAsync() => await MongoCollection.DeleteManyAsync(x=> true);


		public virtual IEnumerable<TEntity> SortBy(Expression<Func<TEntity, object>> predicate)
		{
			var filter = Builders<TEntity>.Filter.Empty;
			var res = MongoCollection.Find(filter).SortBy(predicate);
			return res.ToEnumerable();
		}

		public virtual IEnumerable<TEntity> SortByDescending(Expression<Func<TEntity, object>> predicate)
		{
			var filter = Builders<TEntity>.Filter.Empty;
			var res = MongoCollection.Find(filter).SortByDescending(predicate);
			return res.ToEnumerable();
		}

		public virtual void Drop() 
		{
			MongoCollection.Database.DropCollection(MongoCollection.CollectionNamespace.CollectionName);
		}

		public virtual long Count() => MongoCollection.Count(x => true);

		public virtual long Count(Expression<Func<TEntity, bool>> func) => MongoCollection.Count(func);

		public virtual bool Exists() => MongoCollection.AsQueryable().Any();

		public virtual bool Exists(Expression<Func<TEntity, bool>> predicate) => MongoCollection.AsQueryable().Any(predicate);

		public virtual IEnumerable<TEntity> Search(string search) => MongoCollection.Find(Builders<TEntity>.Filter.Text(search)).ToEnumerable();

		public virtual IEnumerable<TEntity> Where(Expression<Func<TEntity,bool>> func)
		{
			return MongoCollection.Find(func).ToEnumerable();
		}

		public int Sum(Func<TEntity, int> sum) => MongoCollection.Find(Builders<TEntity>.Filter.Empty).ToEnumerable().Sum(sum);

		#region IQueryable<T>

		public virtual IEnumerator<TEntity> GetEnumerator()
		{
			return MongoCollection.AsQueryable().GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return MongoCollection.AsQueryable().GetEnumerator();
		}

		public virtual Type ElementType => MongoCollection.AsQueryable().ElementType;

		public virtual Expression Expression => MongoCollection.AsQueryable().Expression;

		public virtual IQueryProvider Provider => MongoCollection.AsQueryable().Provider;

		#endregion
	}

	public class MongoRepository<TEntity> : MongoRepository<TEntity, string>, IMongoRepository<TEntity>
	   where TEntity : IMongoEntity<string>
	{
		public MongoRepository(IMongoContext context) : base(context)
		{
		}
	}
}
