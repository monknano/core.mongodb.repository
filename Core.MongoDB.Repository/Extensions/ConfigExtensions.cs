﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Core.MongoDB.Repository.Interfaces;
using Core.MongoDB.Repository.Models;

namespace Core.MongoDB.Repository.Extensions
{
	public static class ConfigExtentions
	{
		public static MongoConfigurationBuilder AddMongoRepositories(this IServiceCollection services, Action<MongoDbSetting> setupSetting)
		{
			services.Configure(setupSetting);
			services.AddSingleton<IMongoContext, MongoContext>();
			services.AddTransient(typeof(IMongoRepository<>), typeof(MongoRepository<>));
			return new MongoConfigurationBuilder(services);
		}


	}

}
