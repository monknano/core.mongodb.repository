﻿using Core.MongoDB.Repository.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Core.MongoDB.Repository.Extensions
{
	public class MongoConfigurationBuilder
	{
		public MongoConfigurationBuilder(IServiceCollection services)
		{
			Services = services;
		}

		/// <summary>
		/// Gets the service collection.
		/// </summary>
		public IServiceCollection Services { get; }

		public MongoConfigurationBuilder FromAssembly(Assembly assembly)
		{
			// Repositories.
			var repositoryTypes = assembly.ExportedTypes
										  .Where(t => !t.IsAbstract &&
													  !t.IsInterface &&
													  t.GetInterfaces().Any(i => i.IsGenericType &&
																				 i.GetGenericTypeDefinition() == typeof(IMongoRepository<>)));
			foreach (var repositoryType in repositoryTypes)
			{
				foreach (var i in repositoryType.GetInterfaces().Where(i => !i.IsGenericType))
				{
					Services.AddTransient(i, repositoryType);
				}

			}

			return this;
		}

		public MongoConfigurationBuilder FromAssemblyContaining<T>() => FromAssembly(typeof(T).Assembly);
	}
}
