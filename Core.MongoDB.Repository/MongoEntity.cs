﻿using Core.MongoDB.Repository.Attributes;
using Core.MongoDB.Repository.Interfaces;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Core.MongoDB.Repository
{
	[DataContract]
	[BsonIgnoreExtraElements(Inherited = true)]
	public abstract class MongoEntity : IMongoEntity
	{
		[DataMember]
		[BsonRepresentation(BsonType.ObjectId)]
		public virtual string Id { get; set; }

		[DataMember]
		[AscendingIndex]
		[BsonRepresentation(BsonType.String)]
		public virtual string GuId { get; set; } = Guid.NewGuid().ToString();

	}
}
