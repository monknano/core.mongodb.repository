﻿using Core.MongoDB.Repository.Interfaces;
using Core.MongoDB.Repository.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Reflection;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;

namespace Core.MongoDB.Repository
{
	public class MongoContext : IMongoContext
	{
		private readonly MongoDbSetting _options;

		public MongoContext(IOptions<MongoDbSetting> options)
		{
			_options = options.Value;
			if (!string.IsNullOrEmpty(_options.MongoUrl) || !string.IsNullOrEmpty(_options.Host))
			{
				if (string.IsNullOrEmpty(_options.MongoUrl))
				{
					MongoClientSettings settings = new MongoClientSettings();
					if (string.IsNullOrEmpty(_options.Host))
					{
						throw new ArgumentNullException(nameof(_options.Host), "The cosmos host is null or empty. Can't connect to azure host.");
					}
					settings.Server = new MongoServerAddress(_options.Host, 10255);
					settings.UseSsl = true;
					settings.SslSettings = new SslSettings();
					settings.SslSettings.EnabledSslProtocols = SslProtocols.Tls12;
					if (string.IsNullOrEmpty(_options.DbName))
					{
						throw new ArgumentNullException(nameof(_options.DbName), "Database name is null or empty. Can't get the database.");
					}
					if (string.IsNullOrEmpty(_options.UserName))
					{
						throw new ArgumentException(nameof(_options.UserName), "Must provide a username for connecting to database.");
					}
					MongoIdentity identity = new MongoInternalIdentity(_options.DbName, _options.UserName);
					if (string.IsNullOrEmpty(_options.Password))
					{
						throw new ArgumentNullException(nameof(_options.Password), "Must provide a password for connecting to database.");
					}
					MongoIdentityEvidence evidence = new PasswordEvidence(_options.Password);

					settings.Credential = new MongoCredential("SCRAM-SHA-1", identity, evidence);

					Client = new MongoClient(settings);
					Database = Client.GetDatabase(_options.DbName);
				}
				else
				{
					Client = new MongoClient(_options.MongoUrl);
					Database = Client.GetDatabase(_options.DbName);
				}
			}
			else
			{
				throw new ArgumentNullException("The cosmos host is null or empty. Can't connect to database host.");
			}

		}

		public IMongoClient Client { get; }

		public IMongoDatabase Database { get; }
	}

}
