﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.MongoDB.Repository.Attributes
{
	[AttributeUsage(AttributeTargets.Class)]
	public class CollectionNameAttribute : Attribute
	{
		public CollectionNameAttribute(string value)
		{
			if (string.IsNullOrWhiteSpace(value))
				throw new ArgumentException("Empty collectionname not allowed", nameof(value));
		Name = value;
		}

	public virtual string Name { get; private set; }
}
}
