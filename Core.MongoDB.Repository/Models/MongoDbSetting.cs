﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.MongoDB.Repository.Models
{
	public class MongoDbSetting
	{
		public string Host { get; set; }
		public string MongoUrl { get; set; }
		public string DbName { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }

	}
}
